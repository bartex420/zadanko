package com.bartex.dziedziczenie.osoby.test;

import com.bartex.dziedziczenie.osoby.factory.OsobaFactory;
import com.bartex.dziedziczenie.osoby.model.*;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class OsobaFactoryTest {

    private OsobaFactory osobaFactory;
    private List<Osoba> osoby;

    private final String STUDENT_IMIE = "Jan";
    private final String STUDENT_NAZWISKO = "Kowalski";
    private final int STUDENT_SEMESTR = 2;
    private final int PRACOWNIK_GODZINY = 20;
    private final int PRACOWNIK_STAWKA = 30;
    private final String PRACOWNIK_NAUKOWY_IMIE = "Jerzy";
    private final String PRACOWNIK_NAUKOWY_NAZWISKO = "Dudek";
    private final String PRACOWNIK_NAUKOWY_TYTUL = "Profesor";
    private final String PRACOWNIK_ADMINISTRACYJNY_IMIE = "Adam";
    private final String PRACOWNIK_ADMINISTRACYJNY_NAZWISKO = "Nowak";
    private final String PRACOWNIK_ADMINISTRACYJNY_DZIAL = "Logistyka";

    @Before
    public void setup() {
        this.osobaFactory = new OsobaFactory();
        this.osoby = new ArrayList<>();
        osoby.add(new Student(STUDENT_IMIE, STUDENT_NAZWISKO, STUDENT_SEMESTR));
        osoby.add(new PracownikNaukowy(PRACOWNIK_NAUKOWY_IMIE, PRACOWNIK_NAUKOWY_NAZWISKO,
                PRACOWNIK_GODZINY, PRACOWNIK_STAWKA, PRACOWNIK_NAUKOWY_TYTUL));
        osoby.add(new PracownikAdministracyjny(PRACOWNIK_ADMINISTRACYJNY_IMIE,
                PRACOWNIK_ADMINISTRACYJNY_NAZWISKO, PRACOWNIK_GODZINY, PRACOWNIK_STAWKA, PRACOWNIK_ADMINISTRACYJNY_DZIAL));
    }

    @Test
    public void shouldReturnOnlyStudents() {
        List<Student> students = osobaFactory.getOsobyOfSubclass(osoby, Student.class);
        assertEquals(1, students.size());
        Student student = students.get(0);
        assertEquals(STUDENT_IMIE, student.getImie());
        assertEquals(STUDENT_NAZWISKO, student.getNazwisko());
        assertEquals(STUDENT_SEMESTR, student.getSemestr());
    }

    @Test
    public void shouldReturnOnlyStudentsForEach() {
        List<Student> students = osobaFactory.getOsobyOfSubclassForEach(osoby, Student.class);
        assertEquals(1, students.size());
        Student student = students.get(0);
        assertEquals(STUDENT_IMIE, student.getImie());
        assertEquals(STUDENT_NAZWISKO, student.getNazwisko());
        assertEquals(STUDENT_SEMESTR, student.getSemestr());
    }

    @Test
    public void shouldReturnOnlyStudentsJava8() {
        List<Student> students = osobaFactory.getOsobyOfSubclassJava8(osoby, Student.class);
        assertEquals(1, students.size());
        Student student = students.get(0);
        assertEquals(STUDENT_IMIE, student.getImie());
        assertEquals(STUDENT_NAZWISKO, student.getNazwisko());
        assertEquals(STUDENT_SEMESTR, student.getSemestr());
    }

    @Test
    public void shouldReturnOnlyPracownikNaukowys() {
        List<PracownikNaukowy> pracownicyNaukowi = osobaFactory.getOsobyOfSubclass(osoby, PracownikNaukowy.class);
        assertEquals(1, pracownicyNaukowi.size());
        PracownikNaukowy pracownikNaukowy = pracownicyNaukowi.get(0);
        assertEquals(PRACOWNIK_NAUKOWY_IMIE, pracownikNaukowy.getImie());
        assertEquals(PRACOWNIK_NAUKOWY_NAZWISKO, pracownikNaukowy.getNazwisko());
        assertEquals(PRACOWNIK_NAUKOWY_TYTUL, pracownikNaukowy.getTytulNaukowy());
    }

    @Test
    public void shouldReturnOnlyPracownikNaukowysForEach() {
        List<PracownikNaukowy> pracownicyNaukowi = osobaFactory.getOsobyOfSubclassForEach(osoby, PracownikNaukowy.class);
        assertEquals(1, pracownicyNaukowi.size());
        PracownikNaukowy pracownikNaukowy = pracownicyNaukowi.get(0);
        assertEquals(PRACOWNIK_NAUKOWY_IMIE, pracownikNaukowy.getImie());
        assertEquals(PRACOWNIK_NAUKOWY_NAZWISKO, pracownikNaukowy.getNazwisko());
        assertEquals(PRACOWNIK_NAUKOWY_TYTUL, pracownikNaukowy.getTytulNaukowy());
    }

    @Test
    public void shouldReturnOnlyPracownikNaukowysJava8() {
        List<PracownikNaukowy> pracownicyNaukowi = osobaFactory.getOsobyOfSubclassJava8(osoby, PracownikNaukowy.class);
        assertEquals(1, pracownicyNaukowi.size());
        PracownikNaukowy pracownikNaukowy = pracownicyNaukowi.get(0);
        assertEquals(PRACOWNIK_NAUKOWY_IMIE, pracownikNaukowy.getImie());
        assertEquals(PRACOWNIK_NAUKOWY_NAZWISKO, pracownikNaukowy.getNazwisko());
        assertEquals(PRACOWNIK_NAUKOWY_TYTUL, pracownikNaukowy.getTytulNaukowy());
    }

    @Test
    public void shouldReturnOnlyPracownikAdministracyjnys() {
        List<PracownikAdministracyjny> pracownicyAdministracyjni = osobaFactory.getOsobyOfSubclass(osoby, PracownikAdministracyjny.class);
        assertEquals(1, pracownicyAdministracyjni.size());
        PracownikAdministracyjny pracownikAdministracyjny = pracownicyAdministracyjni.get(0);
        assertEquals(PRACOWNIK_ADMINISTRACYJNY_IMIE, pracownikAdministracyjny.getImie());
        assertEquals(PRACOWNIK_ADMINISTRACYJNY_NAZWISKO, pracownikAdministracyjny.getNazwisko());
        assertEquals(PRACOWNIK_ADMINISTRACYJNY_DZIAL, pracownikAdministracyjny.getDzialAdministracji());
    }

    @Test
    public void shouldReturnOnlyPracownikAdministracyjnysForEach() {
        List<PracownikAdministracyjny> pracownicyAdministracyjni = osobaFactory.getOsobyOfSubclassForEach(osoby, PracownikAdministracyjny.class);
        assertEquals(1, pracownicyAdministracyjni.size());
        PracownikAdministracyjny pracownikAdministracyjny = pracownicyAdministracyjni.get(0);
        assertEquals(PRACOWNIK_ADMINISTRACYJNY_IMIE, pracownikAdministracyjny.getImie());
        assertEquals(PRACOWNIK_ADMINISTRACYJNY_NAZWISKO, pracownikAdministracyjny.getNazwisko());
        assertEquals(PRACOWNIK_ADMINISTRACYJNY_DZIAL, pracownikAdministracyjny.getDzialAdministracji());
    }

    @Test
    public void shouldReturnOnlyPracownikAdministracyjnysJava8() {
        List<PracownikAdministracyjny> pracownicyAdministracyjni = osobaFactory.getOsobyOfSubclassJava8(osoby, PracownikAdministracyjny.class);
        assertEquals(1, pracownicyAdministracyjni.size());
        PracownikAdministracyjny pracownikAdministracyjny = pracownicyAdministracyjni.get(0);
        assertEquals(PRACOWNIK_ADMINISTRACYJNY_IMIE, pracownikAdministracyjny.getImie());
        assertEquals(PRACOWNIK_ADMINISTRACYJNY_NAZWISKO, pracownikAdministracyjny.getNazwisko());
        assertEquals(PRACOWNIK_ADMINISTRACYJNY_DZIAL, pracownikAdministracyjny.getDzialAdministracji());
    }

    @Test
    public void shouldReturnPracownikOnly() {
        List<Pracownik> pracownicy = osobaFactory.getOsobyOfSubclass(osoby, Pracownik.class);
        assertEquals(2, pracownicy.size());
    }

    @Test
    public void shouldReturnPracownikOnlyForeach() {
        List<Pracownik> pracownicy = osobaFactory.getOsobyOfSubclassForEach(osoby, Pracownik.class);
        assertEquals(2, pracownicy.size());
    }

    @Test
    public void shouldReturnPracownikOnlyJava8() {
        List<Pracownik> pracownicy = osobaFactory.getOsobyOfSubclassJava8(osoby, Pracownik.class);
        assertEquals(2, pracownicy.size());
    }
}
