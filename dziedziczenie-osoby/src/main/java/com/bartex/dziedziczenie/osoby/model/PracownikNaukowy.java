package com.bartex.dziedziczenie.osoby.model;

public class PracownikNaukowy extends Pracownik {
    private String tytulNaukowy;

    public PracownikNaukowy(String imie, String nazwisko, int godziny, int stawka, String tytulNaukowy) {
        super(imie, nazwisko, godziny, stawka);
        this.tytulNaukowy = tytulNaukowy;
    }

    public String getTytulNaukowy() {
        return tytulNaukowy;
    }

    public String toString() {
        return (super.toString() + " Tytul Naukowy: " + getTytulNaukowy());
    }
}