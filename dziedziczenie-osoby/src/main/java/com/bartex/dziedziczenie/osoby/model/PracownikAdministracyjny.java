package com.bartex.dziedziczenie.osoby.model;

public class PracownikAdministracyjny extends Pracownik {

    private String dzialAdministracji;

    public PracownikAdministracyjny(String imie, String nazwisko, int godziny, int stawka, String dzialAdministracji) {
        super(imie, nazwisko, godziny, stawka);
        this.dzialAdministracji = dzialAdministracji;
    }

    public String getDzialAdministracji() {
        return dzialAdministracji;
    }

    public String toString() {
        return (super.toString() + " Dzial administracji: " + getDzialAdministracji());
    }
}