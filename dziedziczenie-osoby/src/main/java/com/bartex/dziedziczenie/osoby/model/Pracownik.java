package com.bartex.dziedziczenie.osoby.model;

public abstract class Pracownik extends Osoba {

    private int wyplata;

    public Pracownik(String imie, String nazwisko, int godziny, int stawka) {
        super(imie, nazwisko);
        this.wyplata = stawka * godziny;
    }

    public int getWyplata() {
        return wyplata;
    }

    @Override
    public String toString() {
        return ("\n" + super.toString() + " Wyplata: " + getWyplata());
    }
}