package com.bartex.dziedziczenie.osoby.model;

public class Student extends Osoba {
    private int semestr;

    public Student(String imie, String nazwisko, int semestr) {
        super(imie, nazwisko);
        this.semestr = semestr;
    }

    public int getSemestr() {
        return semestr;
    }

    @Override
    public String toString() {
        return ("\n" + super.toString() + " Nr semestru " + getSemestr());
    }
}