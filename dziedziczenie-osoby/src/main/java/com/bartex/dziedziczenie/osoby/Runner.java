package com.bartex.dziedziczenie.osoby;

import com.bartex.dziedziczenie.osoby.model.Osoba;
import com.bartex.dziedziczenie.osoby.model.PracownikAdministracyjny;
import com.bartex.dziedziczenie.osoby.model.PracownikNaukowy;
import com.bartex.dziedziczenie.osoby.model.Student;

import java.util.ArrayList;
import java.util.List;

public class Runner {

    public static void main(String[] args) {
        List<Osoba> osoby = new ArrayList<>();
        osoby.add(new Student("Jan", "Kowalski", 2));
        osoby.add(new PracownikNaukowy("Jerzy", "Dudek", 20, 20, "profesor"));
        osoby.add(new PracownikAdministracyjny("Jan", "Nowak", 20, 20, "LOGISTYKA"));

        System.out.println(osoby);
    }
}
