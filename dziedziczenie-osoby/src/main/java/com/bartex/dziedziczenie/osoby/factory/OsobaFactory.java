package com.bartex.dziedziczenie.osoby.factory;

import com.bartex.dziedziczenie.osoby.model.Osoba;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class OsobaFactory {

    public <T extends Osoba> List<T> getOsobyOfSubclass(List<Osoba> osoby, Class<T> aClass) {
        List<T> osobyOfSubclass = new ArrayList<>();
        for (int i = 0; i < osoby.size(); i++) {
            if (/*osoby.get(i).getClass().equals(aClass) ||*/ //warunek niepotrzebny, gdyz bledny - nie zwroci instancji klasy Pracownik, gdyz porownujesz dokladne klasy obiektow
                    aClass.isInstance(osoby.get(i))) {
                osobyOfSubclass.add((T) osoby.get(i));
            }
        }
        return osobyOfSubclass;
    }

    public <T extends Osoba> List<T> getOsobyOfSubclassForEach(List<Osoba> osoby, Class<T> aClass) {
        List<T> osobyOfSubclass = new ArrayList<>();
        for (Osoba osoba : osoby) {
            if (aClass.isInstance(osoba)) {
                osobyOfSubclass.add(aClass.cast(osoba));
            }
        }
        return osobyOfSubclass;
    }

    public <T extends Osoba> List<T> getOsobyOfSubclassJava8(List<Osoba> osoby, Class<T> aClass) {
        return osoby.stream()
                .filter(aClass::isInstance)
                .map(aClass::cast)
                .collect(Collectors.toList());
    }
}