package com.bartex.dziedziczenie.figury.model;

public class Trojkat extends Figura {
    private double bokA, bokB, bokC, wysokosc;

    public Trojkat(double bokA, double bokB, double bokC) {
        this.bokA = bokA;
        this.bokB = bokB;
        this.bokC = bokC;
    }

    public Trojkat(double bokA, double wysokosc) {
        this.bokA = bokA;
        this.wysokosc = wysokosc;
    }

    @Override
    public double obliczPole() {
        double pole = bokA * wysokosc;
        if (wysokosc == 0) {
            pole = Math.sqrt((obliczObwod() / 2)
                    * (obliczObwod() / 2 - bokA)
                    * (obliczObwod() / 2 - bokB)
                    * (obliczObwod() / 2 - bokC));
        }
        return pole;
    }

    @Override
    public double obliczObwod() {
        return bokA + bokB + bokC;
    }
}