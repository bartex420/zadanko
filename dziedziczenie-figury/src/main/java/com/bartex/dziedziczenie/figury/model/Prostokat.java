package com.bartex.dziedziczenie.figury.model;

public class Prostokat extends Figura {
    private double bokA;
    private double bokB;

    public Prostokat(double bokA, double bokB) {
        this.bokA = bokA;
        this.bokB = bokB;
    }

    @Override
    public double obliczObwod() {
        return bokA * 2 + bokB * 2;
    }

    @Override
    public double obliczPole() {
        return bokA * bokB;
    }
}