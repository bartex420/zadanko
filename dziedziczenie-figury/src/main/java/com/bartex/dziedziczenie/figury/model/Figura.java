package com.bartex.dziedziczenie.figury.model;

public abstract class Figura {

    public abstract double obliczPole();

    public abstract double obliczObwod();

}