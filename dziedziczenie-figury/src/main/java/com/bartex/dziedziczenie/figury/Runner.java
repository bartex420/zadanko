package com.bartex.dziedziczenie.figury;

import com.bartex.dziedziczenie.figury.model.*;

import java.util.ArrayList;
import java.util.List;

public class Runner {

    public static void main(String[] args) {

        List<Figura> figury = new ArrayList<>();
        figury.add(new Kolo(3));
        figury.add(new Kolo(10));
        figury.add(new Trojkat(4, 4));
        figury.add(new Trojkat(3, 4, 5));
        figury.add(new Kwadrat(2));
        figury.add(new Prostokat(2, 10));
        for (Figura figura : figury) {
            String message = String.format("Pole figury [%S] wynosi: %.2f natomiast obwod: %.2f",
                    figura.getClass().getSimpleName(),
                    figura.obliczPole(),
                    figura.obliczObwod());
            System.out.println(message);
        }
    }
}