package com.bartex.dziedziczenie.figury.model;

public class Kwadrat extends Prostokat {

    public Kwadrat(int bok) {
        super(bok, bok);
    }

    @Override
    public double obliczObwod() {
        return super.obliczObwod();
    }

    @Override
    public double obliczPole() {
        return super.obliczPole();
    }
}