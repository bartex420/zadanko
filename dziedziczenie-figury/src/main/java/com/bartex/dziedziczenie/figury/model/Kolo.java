package com.bartex.dziedziczenie.figury.model;

public class Kolo extends Figura {
    private int promien;

    public Kolo(int promien) {
        this.promien = promien;
    }

    @Override
    public double obliczPole() {
        return promien * promien * Math.PI;
    }

    @Override
    public double obliczObwod() {
        return promien * 2 * Math.PI;
    }
}