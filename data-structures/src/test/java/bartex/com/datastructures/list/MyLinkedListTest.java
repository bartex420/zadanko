package bartex.com.datastructures.list;

import com.bartex.dziedziczenie.osoby.model.Student;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MyLinkedListTest {

    private MyList<Student> osoby;

    @Before
    public void setup() {
        osoby = new MyLinkedList<>();
        osoby.add(new Student("Jan", "Kowalski", 2));
        osoby.add(new Student("Jerzy", "Dudek", 4));

        osoby.add(new Student("Adam", "Nowak", 6));

    }

    @Test
    public void shouldAddElement() {
        assertEquals(3, osoby.size());
        assertTrue(osoby.add(new Student("Anna", "Kwiatkowska", 8)));
        assertEquals(4, osoby.size());
        assertEquals("Anna", osoby.get(3).getImie());
        assertEquals("Kwiatkowska", osoby.get(3).getNazwisko());
        assertEquals(8, osoby.get(3).getSemestr());
    }

    @Test
    public void shouldGetExistingElement() {
        Student student = osoby.get(2);
        assertNotNull(student);
        assertEquals("Adam", student.getImie());
        assertEquals("Nowak", student.getNazwisko());
        assertEquals(6, student.getSemestr());
    }

    @Test
    public void shouldThrowExceptionWhileTryingToGetNotExistingElement() {
        try {
            Student student = osoby.get(100);
            fail("Should throw an IndexOutOfBoundException");
        } catch (IndexOutOfBoundsException ex) {
            assertEquals("Index must be greater than 0 and lower then size.", ex.getMessage());
        }

    }


    @Test
    public void shouldRemoveExistingElement() {
        Student student = osoby.get(1);
        assertEquals(3, osoby.size());
        assertTrue(osoby.remove(student));
        assertEquals("Adam", osoby.get(1).getImie());
        assertEquals(2, osoby.size());


    }

    @Test
    public void shouldNotRemoveNotExistingElement() {
        Student student = new Student("Anna", "Kwiatkowska", 8);
        assertEquals(3, osoby.size());
        assertFalse(osoby.remove(student));
        assertEquals(3, osoby.size());

    }

    @Test
    public void shouldSetExistingElement() {


        assertTrue(osoby.set(1, osoby.get(2)));
        shouldDisplay();
        assertEquals(osoby.get(2), osoby.get(1));
        assertEquals("Adam", osoby.get(1).getImie());
        assertEquals("Adam", osoby.get(2).getImie());
        assertEquals("Jan", osoby.get(0).getImie());
        assertEquals(3, osoby.size());
    }

    @Test
    public void shouldDisplay() {
        System.out.println(osoby.toString());

    }
}
