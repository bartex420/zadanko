package bartex.com.datastructures.list;


import java.util.List;

public interface MyList<T> {


    boolean add(T element);

    boolean remove(T element);

    T get(int index) throws IndexOutOfBoundsException;

    boolean set(int index, T element) throws IndexOutOfBoundsException;

    int size();

    MyIterator<T> getIterator();

}
