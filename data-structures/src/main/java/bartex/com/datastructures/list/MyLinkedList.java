package bartex.com.datastructures.list;


public class MyLinkedList<T> implements MyList<T> {
    int size;


    private Node<T> node;


    @Override
    public boolean add(T element) {
        /*MyLinkedList newLink = new MyLinkedList(element);
        newLink.next = first;

        first = newLink;

        size++;


        return true;*/
        boolean isAdded = false;
        if (size == 0) {
            node = new Node<>();
            node.setValue(element);
            isAdded = true;
            size++;
        } else {
            Node<T> tempNode = node;
            while (tempNode.getNext() != null) {
                tempNode = tempNode.getNext();
            }
            Node<T> next = new Node<>();
            next.setValue(element);
            tempNode.setNext(next);
            isAdded = true;
            size++;
        }
        return isAdded;

    }

    @Override
    public boolean remove(T element) {
        boolean isRemoved = false;
        Node<T> tempNode = node;
        Node<T> previousNode = node;


        while (tempNode.getNext() != null) {
            previousNode = tempNode;
            tempNode = tempNode.getNext();

            if (tempNode.getValue().equals(element)) {
                previousNode.setNext(tempNode.getNext());
                size--;
                isRemoved = true;
                break;
            } else if (previousNode.getValue().equals(element)) {
                previousNode.setValue(tempNode.getValue());
                previousNode.setNext(tempNode.getNext());
                size--;
                isRemoved = true;
                break;
            }


        }

        return isRemoved;
    }


    @Override
    public T get(int index) throws IndexOutOfBoundsException {
        /*MyLinkedList current = first;

        while (index > 1) {
            if (current != null) {
                current = current.next;
            }
            index--;


        }


        return (T) current;*/
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Index must be greater than 0 and lower then size.");
        }
        T value = null;
        Node<T> tempNode = node;
        for (int i = 0; i < size; i++) {
            if (index == i) {
                value = tempNode.getValue();
                break;
            }
            tempNode = tempNode.getNext();
        }
        return value;
    }

    @Override
    public boolean set(int index, T element) throws IndexOutOfBoundsException {
        Node<T> tempNode = node;

        boolean isSet = false;
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Index must be greater than 0 and lower then size.");
        }
        if (index == 0) {
            node.setValue(element);
            isSet = true;

        }
        if (index < size && index > 0) {

            while (tempNode.getNext() != null && index > 0) {

                tempNode = tempNode.getNext();
                index--;
            }
            tempNode.setValue(element);
            isSet = true;


        }


        return isSet;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public MyIterator getIterator() {


        return null;

    }

    @Override
    public String toString() {
        Node<T> tempNode = node;
        String display = " ";
        for (int currentNode = 0; currentNode < size; currentNode++) {
            display += currentNode + " " + tempNode.toString() + "\n";
            tempNode = tempNode.getNext();

        }


        return display;
    }
}
