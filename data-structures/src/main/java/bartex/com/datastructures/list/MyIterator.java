package bartex.com.datastructures.list;

public interface MyIterator<T> {
    boolean hasNext();

    boolean hasPrevious();

    T next();
}
